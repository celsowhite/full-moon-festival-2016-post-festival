$(document).ready(function(){
  
  /*======================== 
  NAVIGATION
  ========================*/

  /*=== Main Header Transition ===*/

  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if(scroll >= 30) {
      $('.main_header').addClass('scrolled');
    }
    else {
      $('.main_header').removeClass('scrolled');
    }
  });

  /*=== Smooth Scroll ===*/

  // Desktop nav and down arrow

  $('.main_navigation li a, .scroll_down_arrow a').smoothScroll({
    speed: 1000,
  });

  // Mobile Nav

  $('.mobile_navigation_list li a').smoothScroll({
    speed: 1000,
    offset: -30
  });

  /*=== Navigation Indicators based on Scroll Position ===*/

  $(window).scroll(function(){

    var lineupPosition = $('#lineup_panel').offset().top - 30;
    var galleryPosition = $('#gallery_panel').offset().top - 30;
    var aboutPosition = $('#about_panel').offset().top - 30;
    var scroll = $(this).scrollTop();

    if(lineupPosition <= scroll && scroll <= galleryPosition) {
      $('.main_navigation li a').removeClass('active');
      $('#lineup_link').addClass('active');
    }
    else if (galleryPosition <= scroll && scroll <= aboutPosition) {
      $('.main_navigation li a').removeClass('active');
      $('#gallery_link').addClass('active');
    }
    else if (aboutPosition <= scroll) {
      $('.main_navigation li a').removeClass('active');
      $('#about_link').addClass('active');
    }
    else {
      $('.main_navigation li a').removeClass('active');
    }

  });

  /*======================== 
  MOBILE NAVIGATION
  ========================*/

  /*=== Open the fullscreen overlay ===*/

  $('.menu_icon').click(function(){
    $('.mobile_navigation').fadeIn('fast');
  });

  /*=== Clicking the x within the overlay closes it ===*/

  $('.close_icon').click(function(){
    $('.mobile_navigation').fadeOut('fast');
  });

  /*=== Clicking a navigation link within the overlay scrolls to the section of the page and closes it ===*/

  $('.mobile_navigation_list li a').click(function(){
    $('.mobile_navigation').fadeOut('fast');
  });

  /*======================== 
  RECAP VIDEO
  ========================*/

  // Video Variables

  var recapVideoContainer = $('.video_recap');
  var recapVideoIframe = $('.video_recap iframe')[0];
  var player = new Vimeo.Player(recapVideoIframe);

  // Fitvid initialization to ensure video is responsive.

  $('.video_container').fitVids();

  // Button to trigger the video popup and the video playing.

  $('.video_recap_trigger').click(function(){
    recapVideoContainer.fadeIn(800, function(){
      player.play();
    });
  });

  // Button to close the video popup and set the video back to it's initial state.

  $('.video_recap .close_icon').click(function(){
    recapVideoContainer.fadeOut(800, function(){
      player.unload();
    });
  });

  /*================================= 
  MAILCHIMP AJAX
  =================================*/

  // Set up form variables

  var mailchimpForm = $('.mc-embedded-subscribe-form');

  // On submit of the form send an ajax request to mailchimp for data.

  mailchimpForm.submit(function(e){

    // Set variables for this specific form

    var that = $(this);
    var mailchimpSubmit = $(this).find('input[type=submit]');
    var errorResponse = $(this).closest('.mc_embed_signup').find('.mce-error-response');
    var successResponse = $(this).closest('.mc_embed_signup').find('.mce-success-response');

    // Make sure the form doesn't link anywhere on submit.

    e.preventDefault();

    // JQuery AJAX request http://api.jquery.com/jquery.ajax/

    $.ajax({
      method: 'GET',
      url: that.attr('action'),
      data: that.serialize(),
      dataType: 'jsonp',
      success: function(data) {
      // If there was an error then show the error message.
      if (data.result === 'error') {
        // Hide the first few characters int the error message string which display the error code and hyphen.
        var messageWithoutCode = data.msg.slice(3);
        errorResponse.text(messageWithoutCode).show(300).delay(3000).hide(300);
      }
      // If success then show message
      else {
        successResponse.text('Success! Please check your email for a confirmation message.').show(300).delay(3000).hide(300);
      }
      }
    });
  });

});